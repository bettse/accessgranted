require("dotenv").config();
const util = require("util");
const fs = require("fs");
const PDFDocument = require("pdfkit");
const ipp = require("ipp");
const QRCode = require("qrcode");

const SocketHookClient = require("./SocketHookClient");

const { SOCKETHOOK_HASH } = process.env;
const { RECEIPT_PRINTER_URI } = process.env;
const { NODE_ENV } = process.env;
const { LOOKUP } = process.env;
const uidMap = {};
LOOKUP.split(",").forEach((line) => {
  const [uid, name] = line.split(":");
  uidMap[uid.toLowerCase()] = name;
});

const timeZone = "America/Los_Angeles";

//media=custom_71.97x99.84mm_71.97x99.84mm'
const receiptPt = {
  height: 283,
  width: 204,
};
const margin = 10;

const sockethookClient = new SocketHookClient(SOCKETHOOK_HASH, newRequest);
const receiptPrinter = ipp.Printer(RECEIPT_PRINTER_URI);
const execute = util.promisify(receiptPrinter.execute.bind(receiptPrinter));
const dOptions = {
  timeZone,
  year: "2-digit",
  month: "2-digit",
  day: "2-digit",
};
const tOptions = { timeZone, hour12: false };

async function printReceipt(pdf) {
  try {
    if (NODE_ENV === "development") {
      const attrs = await execute("Get-Printer-Attributes", null);
      console.log(attrs["printer-attributes-tag"]);
      fs.writeFileSync("./receipt.pdf", pdf);
      return;
    }

    // 'custom_71.97x99.84mm_71.97x99.84mm',
    // 'custom_71.97x199.67mm_71.97x199.67mm',
    // 'custom_71.97x1499.66mm_71.97x1499.66mm',
    const request = {
      "operation-attributes-tag": {
        "requesting-user-name": "bettse@fastmail.fm",
        "job-name": "receipt.pdf",
        "document-format": "application/pdf",
      },
      "job-attributes-tag": {
        media: "custom_71.97x99.84mm_71.97x99.84mm",
        //'orientation-requested-supported': [ 'portrait', 'landscape', 'reverse-landscape', 'reverse-portrait' ],
        "orientation-requested": "reverse-portrait",
      },
      data: pdf,
    };

    const result = execute("Print-Job", request);
  } catch (e) {
    console.error("error printing", e);
  }
}

async function composePdf(event) {
  const { uid, count } = event;

  const who = uidMap[uid.toLowerCase()];
  const now = new Date();
  const time = now.toLocaleTimeString("en-US", tOptions);
  const date = now.toLocaleDateString("en-US", dOptions);

  console.log({uid, count, who, time, date});

  return new Promise(async (resolve, reject) => {
    const doc = new PDFDocument({
      size: [receiptPt.width, receiptPt.height],
      margin,
    });

    let buffers = [];
    doc.on("error", reject); //dunno if it actually emits this
    doc.on("data", buffers.push.bind(buffers));
    doc.on("end", () => {
      const pdfData = Buffer.concat(buffers);
      resolve(pdfData);
    });

    doc.text("ACCESS GRANTED", 0, margin, {
      align: "center",
      width: receiptPt.width,
      height: receiptPt.height,
    });
    doc.fontSize(8);
    doc.text("https://accessgranted.ericbetts.dev", {
      align: "center",
    });

    doc.moveDown();

    doc.text(date, {
      align: "left",
      continued: true,
    });
    doc.text(time, {
      align: "right",
    });
    doc.moveDown();

    // -------
    doc
      .moveTo(margin, doc.y)
      .lineTo(receiptPt.width - margin - margin, doc.y)
      .dash(5, { space: 5 })
      .stroke();
    doc.moveDown();

    doc.text(`${uid} (${who})`, {
      align: "left",
      continued: true,
    });
    doc.text(count, {
      align: "right",
    });
    doc.moveDown();

    // -------
    doc
      .moveTo(margin, doc.y)
      .lineTo(receiptPt.width - margin - margin, doc.y)
      .dash(5, { space: 5 })
      .stroke();
    doc.moveDown();

    const qr = await QRCode.toDataURL(uid);
    const qrSize = 128;
    doc.image(qr, receiptPt.width / 2 - qrSize / 2, doc.y, {
      fit: [qrSize, qrSize],
      align: "center",
      valign: "center",
    });

    doc.end();
  });
}

async function newRequest(event) {
  const { uid, count } = event;

  const pdf = await composePdf(event);
  await printReceipt(pdf);
}

//newRequest({uid: '047A892A506380', count: 110})
