# Access Granted Receipts

#### Service

1. `sudo cp accessgranted.service /lib/systemd/system/`
2. `sudo chmod 644 /lib/systemd/system/accessgranted.service`
3. `sudo systemctl daemon-reload`
4. `sudo systemctl enable accessgranted.service`
5. `sudo systemctl start accessgranted.service`

`journalctl --since today -f -u accessgranted.service`
