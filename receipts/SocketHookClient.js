require("dotenv").config();
const { WebSocket } = require("ws");
const fetch = require("node-fetch");
const debug = require("debug")("SocketHookClient");

const wsUrl = "wss://websocket.ericbetts.dev";

class SocketHookClient {
  constructor(key, handler) {
    if (!key || !handler) {
      throw new Error("missing key or handler");
    }
    this.key = key;
    this.handler = handler;

    this.ws = new WebSocket(wsUrl);
    this.ws.on("open", this.onOpen.bind(this));
    this.ws.on("error", this.onError.bind(this));
    this.ws.on("close", this.onClose.bind(this));
  }

  onError(e) {
    debug("ws error", e);
  }

  onOpen() {
    debug("ws open");
    const heartbeat = setInterval(() => {
      this.ws.ping();
    }, 5 * 60 * 1000);

    const cmd = {
      action: "subscribe",
      key: this.key,
    };

    this.ws.send(JSON.stringify(cmd));
    this.ws.on("message", this.onMessage.bind(this));
    this.ws.on("close", () => {
      clearInterval(heartbeat);
    });

    // AWS websocket (which Sockethook uses) has a max connection limit of 2 hours
    //setTimeout(this.reconnect.bind(this), 2 * 60 * 60 * 1000);
  }

  async onMessage(message) {
    const data = message.toString("ascii");
    let parsed;
    try {
      parsed = JSON.parse(data);
    } catch (e) {
      console.log("on message error", data, "\n", e);
      return;
    }
    const { action, state } = parsed;
    //Ignore subscribed message
    if (state && state === "subscribed") {
      return;
    }
    if (parsed) {
      this.handler(parsed);
    }
  }

  reconnect() {
    debug("reconnect");
    this.ws.terminate();

    this.ws = new WebSocket(wsUrl);
    this.ws.on("open", this.onOpen.bind(this));
    this.ws.on("error", this.onError.bind(this));
    this.ws.on("close", this.onClose.bind(this));
  }

  onClose() {
    debug("ws close");
    clearTimeout(this.pingTimeout);
    this.reconnect();
  }

  static async send(path, obj) {
    const url = `https://sockethook.ericbetts.dev/hook/${path}`;
    try {
      const response = await fetch(url, {
        method: "POST",
        body: JSON.stringify(obj),
      });
    } catch (e) {
      console.log("upload error", e);
    }
  }
}

module.exports = SocketHookClient;
