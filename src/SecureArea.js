import React, {useEffect, useState} from 'react';

import {When} from 'react-if';
import Alert from 'react-bootstrap/Alert'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'

const { location } = window;
const { search } = location;
const params = new URLSearchParams(search);

function SecureArea(props) {
  const { children } = props;
  const [valid, setValid] = useState(null);
  const [count, setCount] = useState(null);
  const [who, setWho] = useState('');

  const counter = params.get('ctr')
  const uid = params.get('uid')
  const cmac = params.get('c')

  useEffect(() => {
    async function validateCmac() {
      try {
        const response = await fetch('/.netlify/functions/validateCmac');
        setValid(response.ok);
        const data = await response.json();
        const { count, who = '' } = data; // replays
        setCount(count);
        setWho(who);
        window.history.pushState({}, document.title, "/");
      } catch(e) {
        console.log(e);
        setValid(false);
      }
    }
    if (counter && uid && cmac) {
      validateCmac();
    } else {
      // Avoid network call when params are missing
      setValid(false);
    }
  }, [counter, uid, cmac]);

  if (valid === null) {
    return (
      <div>Loading...</div>
    );
  }

  if (!valid) {
    return (
      <>
        <Row className="justify-content-center">
          <Col md={12} lg={6}>
            <Card>
              <Card.Header>You are not currently authorized.</Card.Header>
              <Card.Body>
                <Card.Text>
                  Please use your Access Granted badge to generate a valid URL.  If you do not have an Access Granted badge, contact me to apply for one.
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </>
    );
  }

  //Valid
  return (
    <>
      <When condition={who && who.length > 0}>
        <h1>Hello {who}</h1>
      </When>
      <When condition={count !== null && count > 0}>
        <Alert variant="warning">
          This UID/Counter pair has been used {count} times before
        </Alert>
      </When>
      {children}
    </>
  );
}

export default SecureArea;
