import React from 'react';

import { When } from 'react-if';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

import ProjectList from './ProjectList';
import SecureArea from './SecureArea';

import './App.css';

const HEX = 0x10;

const { location } = window;
const { search } = location;
const params = new URLSearchParams(search);

function App() {
  const counter = parseInt(params.get('ctr'), HEX);
  const uid = params.get('uid')

  return (
    <div className="App">
      <Navbar bg="secondary">
        <Navbar.Brand>Access Granted</Navbar.Brand>
        <When condition={!!uid}><Nav.Item className="pr-3">UID: {uid}</Nav.Item></When>
        <When condition={!!counter}><Nav.Item className="pr-3">Counter: {counter}</Nav.Item></When>
      </Navbar>
      <Container className="text-center pt-5" fluid='md'>
        <SecureArea/ >
        <ProjectList/>
      </Container>
      <footer className="footer font-small mx-auto pt-5">
        <Container fluid className="text-center">
          <Row noGutters>
            <Col>
            </Col>
          </Row>
        </Container>
      </footer>
    </div>
  );
}

export default App;
