import React from 'react';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import ListGroup from 'react-bootstrap/ListGroup';
import Alert from 'react-bootstrap/Alert';

import {If, Then, Else} from 'react-if';

const projects = [{
  name: 'Meeseeks Box',
  subdomain: 'meeseeks-box',
  description: "Named for a character on “Rick & Morty”; A Raspberry Pi Zero with a speaker and a button will play a random clip of the character when you hit the button. The website acts as a virtual button: press it and the speaker in my living room will play a clip just like if you hit the real, physical, button.",
  access: "Partial: There is a 5 minute lockout for anyone not using Access Granted",
}, {
  name: 'Joy',
  subdomain: 'joy',
  description: "Using machine learning and a camera pointed at where I stand while using the computer at home or work, see my joy score. More details on the page.",
},{
  name: 'Family Emoji Maker',
  subdomain: 'family',
},{
  name: 'Sockethook',
  description: 'Webhook to websocket gateway',
  subdomain: 'sockethook',
},{
  name: 'iClass decrypter',
  subdomain: 'iclass',
},{
  name: 'PPPP string decoder',
  subdomain: 'ppppdecodestring',
},{
  name: 'PAW Prices',
  subdomain: 'pawprices',
},{
  name: 'UHF Ad Wall',
  subdomain: 'adwall',
}]

const { location } = window;

function buildItem({name, subdomain, description = '', access='full'}) {
  const url = `https://${location.hostname.replace('accessgranted', subdomain)}`
  return (
    <ListGroup.Item key={subdomain} action href={url}>
    <Row>
      <Col xs={4}>
        <h3>{name}</h3>
      </Col>
      <Col xs={8}>
        <If condition={access === 'full'}>
          <Then>
            <Alert variant="success">Full Access</Alert>
          </Then>
          <Else>
            <Alert variant="warning">{access}</Alert>
          </Else>
        </If>
      </Col>
    </Row>

    <Row className="justify-content-left text-left">
      <description>{description}</description>
    </Row>
    </ListGroup.Item>
  );
}

function ProjectList() {
  return (
    <Row>
      <Col xs={12}>
        <h2>Project List</h2>
        <ListGroup>
          {projects.map(buildItem)}
        </ListGroup>
      </Col>
    </Row>
  );
}

export default ProjectList;
