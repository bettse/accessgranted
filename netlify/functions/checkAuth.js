const cookie = require("cookie");
const { verify } = require("jsonwebtoken");

const { NODE_ENV, JWT_SECRET } = process.env;

const failure = {
  statusCode: 503,
  body: JSON.stringify({}),
};

async function checkAuth(headers, role = 'user') {
  if (!headers.cookie) {
    throw new Error("No Cookies");
  }

  const cookies = cookie.parse(headers.cookie);
  if (!cookies || !cookies.nf_jwt) {
    throw new Error("No JWT");
  }
  const { nf_jwt } = cookies;
  const decoded = await verify(nf_jwt, JWT_SECRET);
  const { app_metadata } = decoded;
  const { authorization } = app_metadata;
  const { roles } = authorization;
  if (!roles.includes(role)) {
    throw new Error(`Not a ${role}`);
  }
}

exports.handler = async function (event, context) {
  context.callbackWaitsForEmptyEventLoop = false;
  const { httpMethod, headers, queryStringParameters } = event;
  const { role = 'user' } = queryStringParameters;
  console.log({headers});
  if (httpMethod === "OPTIONS" || headers['x-forwarded-method'] == "OPTIONS") {
    return {
      statusCode: 204,
    };
  }
  try {
    await checkAuth(headers, role);
  } catch (e) {
    console.log('Exception', e);
    return {
      statusCode: 401,
      body:
        httpMethod === "HEAD" ? undefined : JSON.stringify({ msg: e.message }),
    };
  }

  return {
    statusCode: 204,
  };
};
