const { aesCmac } = require("node-aes-cmac");
const cookie = require('cookie')
const { sign } = require('jsonwebtoken');
const fetch = require("node-fetch");

const {
  InfluxDB,
  Point,
  typeSerializers
} = require("@influxdata/influxdb-client");
typeSerializers["dateTime:RFC3339"] = dt => new Date(dt).getTime();

const {
  INFLUXDB_V2_BUCKET,
  INFLUXDB_V2_TOKEN,
  INFLUXDB_V2_ORG,
  INFLUXDB_V2_URL
} = process.env;

const { JWT_SECRET, MASTER_KEY_HEX, NETLIFY_DEV, ADMIN_UIDS } = process.env;
const { ADA_IO_KEY } = process.env;
const { SOCKETHOOK_SECRET } = process.env;

const { LOOKUP } = process.env;
const uidMap = {};
LOOKUP.split(",").forEach((line) => {
  const [uid, name] = line.split(":");
  uidMap[uid.toLowerCase()] = name;
});

const masterKey = Buffer.from(MASTER_KEY_HEX, "hex");
const admin_uids = ADMIN_UIDS.split(',')

const system_id = Buffer.from("accessgranted");
const keyNo = 0x00;
const NOT_FOUND = -1;
const hour = 3600000
const twoWeeks = 14 * 24 * hour
const ttl = 24 * 60 * 60

const failure = {
  statusCode: 401,
  body: JSON.stringify({})
};

const error = {
  statusCode: 503,
  body: JSON.stringify({})
};

const writeApi = new InfluxDB({
  url: INFLUXDB_V2_URL,
  token: INFLUXDB_V2_TOKEN
}).getWriteApi(INFLUXDB_V2_ORG, INFLUXDB_V2_BUCKET, "ns", {
  writeFailed: console.log
});
writeApi.useDefaultTags({ env: NETLIFY_DEV ? 'dev' : 'prod'});

const queryApi = new InfluxDB({
  url: INFLUXDB_V2_URL,
  token: INFLUXDB_V2_TOKEN
}).getQueryApi(INFLUXDB_V2_ORG);

function mact(mac) {
  const t = Buffer.alloc(8);
  for (var i = 0; i < mac.length; i++) {
    if (i % 2 == 1) {
      t[(i / 2) >>> 0] = mac[i];
    }
  }
  return t;
}

function MAC(key, data) {
  return aesCmac(key, data, { returnAsBuffer: true });
}

function AES128KeyDiversification(m, masterKey) {
  const divConst = 0x01;
  const d = Buffer.from([divConst, ...m]);
  return MAC(masterKey, d);
}

function validateCmac(url) {
  try {
    const offset = url.indexOf(".ericbetts.dev");
    if (offset === NOT_FOUND) {
      return false;
    }
    const parsedUrl = new URL(url);
    const { searchParams } = parsedUrl;
    const counter = parseInt(searchParams.get("ctr"), 0x10);
    const cmac = searchParams.get("c");
    const uid = searchParams.get("uid");
    const UID = Buffer.from(uid, "hex");
    const dynamicFileData = url.slice(offset, url.length - 16);

    const m = Buffer.from([...UID, keyNo, ...system_id]);
    const key = AES128KeyDiversification(m, masterKey);

    const sv2 = Buffer.from("3CC300010080" + uid + "000000", "hex");
    sv2.writeUIntLE(counter, 13, 3);
    const SesSDMFileReadMAC = MAC(key, sv2);
    const SDMMACfull = MAC(SesSDMFileReadMAC, dynamicFileData);
    const t = mact(SDMMACfull);

    return cmac.toLowerCase() === t.toString("hex").toLowerCase();
  } catch (e) {
    console.log(e);
    return false;
  }
}

function getHighest(uid, service) {
  const query = `
  from(bucket: "${INFLUXDB_V2_BUCKET}")
  |> range(start: -30d)
  |> filter(fn: (r) => r["_measurement"] == "validation")
  |> filter(fn: (r) => r["_field"] == "counter")
  |> filter(fn: (r) => r["env"] == "${NETLIFY_DEV ? "dev" : "prod"}")
  |> filter(fn: (r) => r["uid"] == "${uid}")
  |> filter(fn: (r) => r["service"] == "${service}")
  |> max(column: "_value")
  `;
}

async function getReplay(uid, counter) {
  const query = `
  from(bucket: "${INFLUXDB_V2_BUCKET}")
  |> range(start: -30d)
  |> filter(fn: (r) => r["_measurement"] == "validation")
  |> filter(fn: (r) => r["_field"] == "counter")
  |> filter(fn: (r) => r["env"] == "${NETLIFY_DEV ? "dev" : "prod"}")
  |> filter(fn: (r) => r["uid"] == "${uid}")
  |> drop(columns: ["service"])
  |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
  |> filter(fn: (r) => r["counter"] == ${counter})
  |> count(column: "counter")
  `;

  try {
    const results = await queryApi.collectRows(query);
    if (results.length < 1) {
      return 0;
    }
    const [result] = results;
    const { counter } = result;
    return counter;
  } catch (err) {
    console.log(err);
    return -1;
  }
}

const timeZone = 'America/Los_Angeles';
async function messageboard(uid, count) {
  const dOptions = { timeZone, year: '2-digit', month: '2-digit', day: '2-digit' };
  const tOptions = { timeZone, hour12: false };
  const now = new Date();
  const time = now.toLocaleTimeString('en-US', tOptions);
  const date = now.toLocaleDateString('en-US', dOptions);
  const value = `{2}${uid}{0}:{4}${count}\n{6}${time}\n{8}${date}`
  await fetch('https://io.adafruit.com/api/v2/bettse/feeds/accessgranted/data', {
    method: 'POST', // or 'PUT'
    headers: {
      'Content-Type': 'application/json',
      'X-AIO-Key': ADA_IO_KEY,
    },
    body: JSON.stringify({value}),
  });

  await fetch(`https://sockethook.ericbetts.dev/hook/accessgranted/${SOCKETHOOK_SECRET}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({uid, count}),
  });
}

exports.handler = async function(event, context) {
  const { headers } = event;
  let { referer } = headers;
  try {
    if (!referer || referer === "") {
      console.log("referer missing or empty");
      return failure;
    }
    if (NETLIFY_DEV) {
      referer = referer.replace(
        "http://localhost:9000",
        "https://accessgranted.ericbetts.dev"
      );
    }

    const parsedUrl = new URL(referer);
    const { searchParams, hostname } = parsedUrl;
    const service = hostname.split(".").shift();
    const counter = parseInt(searchParams.get("ctr"), 0x10);
    const uid = searchParams.get("uid");
    // If the cmac didn't involve dynamicFileData, i'd just pass uid/counter in
    const valid = validateCmac(referer);
    const count = await getReplay(uid, counter);

    // TODO: query influx for last used counter value
    // Log to influx
    const point = new Point("validation")
      .tag("uid", uid || "unknown")
      .tag("service", service || "unknown")
      .uintField("counter", counter)
      .booleanField("valid", valid);

    await writeApi.writePoint(point);
    //await writeApi.close();
    await writeApi.flush();
    const roles = ['user'];
    if (admin_uids.includes(uid)) {
      roles.push('admin');
    }

    if (valid) {
      const userData = {
        sub: uid,
        exp: Math.floor(Date.now() / 1000) + ttl,
        app_metadata: {
          authorization: {
            roles,
          },
        },
      };

      const jwt = sign(userData, JWT_SECRET);
      const myCookie = cookie.serialize('nf_jwt', jwt, {
        secure: !NETLIFY_DEV,
        httpOnly: true,
        path: '/',
        domain: NETLIFY_DEV ? 'localhost' : '.ericbetts.dev',
        maxAge: twoWeeks,
      })
      const headers = {
        "Content-Type": "application/json",
        'Cache-Control': 'no-cache',
      }

      if (count === 0) {
        headers['Set-Cookie'] = myCookie;
        await messageboard(uid, counter)
      }
      const who = uidMap[uid.toLowerCase()];

      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          uid,
          valid,
          count,
          who,
        })
      };
    }
  } catch (e) {
    console.log(e);
    return error;
  }
  console.log('fallthrough')
  return failure;
};
